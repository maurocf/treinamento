# PRINT e um comando utilizado para exibir mensagens na tela.

print("Ola, bem vindo ao curso de pyton!")

# INPUT e um comando utilizado para coletar informacoes do usuario.

input("Qual e o seu nome? ")

# VARIAVEIS sao palavras q armazenam um valor.

nome = "Mauro de Camargo Filho"
print(nome)

idade = input("Qual a sua idade? ")
print(idade)