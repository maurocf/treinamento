#================================================================

numero1 = int(input("Qual o primeiro numero da sua escolha? "))
numero2 = int(input("Qual o segundo numero da sua escolha? "))

print(f"Soma: {numero1} + {numero2} = ", numero1 + numero2)
print(f"Diferenca: {numero1} - {numero2} = ", numero1 - numero2)

#=================================================================

frase = "Umxpratoxdextrigoxparaxtresxtigresxtristes".replace ("x", " " )
print(frase)

#=================================================================

nascimento = int(input("Qual o ano do seu nascimento? "))

if nascimento > 1995 :
    print(f"ano de nascimento = {nascimento}: Geracao: Z ")
elif nascimento > 1980 :
    print(f"ano de nascimento = {nascimento}: Geracao: Y ")
elif nascimento > 1965 :
    print(f"ano de nascimento = {nascimento}: Geracao: X ")
else:
    print(f"ano de nascimento = {nascimento}: Geracao: Baby Boomer ")        

#=================================================================