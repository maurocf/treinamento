# Tipos Primitivos

# Strings -> str -> Frases, conjuntos de caracteres, sempre entre aspas.
# Integer -> int -> Numeros interiros, sem aspas.
# Float -> float -> Numeros decimais, numeros reais, numeros com ponto, sem aspas.
# Boolean -> bool -> Tru ou False, sem pastas.

# nomee = "Mauro Camargo Filho"   # String
# peso = "100"                    # String
# idade = 47                      # Integer
# altura = 1.74                   # Float
# moreno = True                   # Boolean

# #=================================================================

# numero1 = "15"
# numero2 = "25"

# numero3= numero1 + numero2
# print(numero3)

#=================================================================

# idade = input("Qual o sua idade? ")

# idade = int(idade)

# print(idade)

#=================================================================

# Estrutura de decisao

# idade = input(input("Qual sua idade? "))

# if idade > 17: 
#     print("Voce pode entrar e comprar bebida no bar.")
#     print("Aproveite a noite! ")

# elif idade > 15:
#     print("Voce pode entrar, mas nao pode comprar bebidas no bar. ")     

# else:
#     print("Voce e muito jovem para entrar.")


#=================================================================

# Estruturas de repeticao

# FOR e WHILE

# contador = 10
# while contador != 0:
#     print("Estou contando...") 
#     print("Executando codigo...")
#     contador = contador - 1
#     contador = int(input("Digite um numero: "))

while True:
    resposta = input("Quer parar o programa? [s/n]: ")
    if resposta == "s":
       break